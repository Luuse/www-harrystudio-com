// --------------À PROPOS-------------- //
function showHtmlDiv(id) {
  const htmlShow = document.getElementById(id);

  if (htmlShow.style.display === "none") {
    htmlShow.style.display = "block";
  } else {
    htmlShow.style.display = "none";
  }
}

// -------------- MEDIA QUERY -------------- //
function checkCategories(query) {
  // Quand la nav est cachée (on est sur un ordi dans une petite fenêtre)
  //  et qu'on agrandit la fenêtre, il faut que la nac réapparaisse
  if (query.matches) {
    nav.style.display = "block";
  } else {
    nav.style.display = "none";
  }
}

const nav = document.getElementById("categories");
let x = window.matchMedia("(min-width: 800px)");
x.addListener(checkCategories);
checkCategories(x);

// --------------SLIDER-------------- //
let slideIndex = [];
let slideId = [];

let slider = document.getElementsByClassName("project");
// console.log(slider.length);

for (var i = 0; i < slider.length; i++) {
  slideId.push("mySlides" + i);
  slideIndex.push(1);
}

for (var i = 0; i < slider.length; i++) {
  // console.log(i);
  showSlides(1, i);
}

function plusSlides(n, no) {
  showSlides((slideIndex[no] += n), no);
}

function showSlides(n, no) {
  let x = document.getElementsByClassName(slideId[no]);

  // console.log(no, x[0])

  if (n > x.length) {
    slideIndex[no] = 1;
  }
  if (n < 1) {
    slideIndex[no] = x.length;
  }
  for (let i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[slideIndex[no] - 1].style.display = "block";
}

// -------------- OPENING SLIDERS -------------- //
const sliders = document.querySelectorAll(".thumbnail");
let openedSlider = null;

for (let el of sliders) {
  el.addEventListener("click", (ev) => {
    let container;
    container = ev.target.parentElement.parentElement.querySelector(
      ".slideshow-container"
    );
    container.style.display = "block";

    openedSlider = container;
  });
}

// -------------- CLOSING SLIDERS -------------- //
const closeSliders = document.getElementsByClassName("closing");

for (let close of closeSliders) {
  close.addEventListener("click", (ev) => {
    ev.target.parentElement.style.display = "none";
    openedSlider = null;
  });
}

// -------------- KEYBOARD SLIDERS -------------- //
document.addEventListener("keydown", (event) => {
  // event = event || window.;

  if (openedSlider != null) {
    const sliderIndex = openedSlider.children[1].classList[0].replace(
      "mySlides",
      ""
    );

    if (event.keyCode == "37") {
      // 37 = left
      plusSlides(-1, sliderIndex);
    } else if (event.keyCode == "39") {
      // 39 = right
      plusSlides(1, sliderIndex);
    } else if (event.keyCode == "27") {
      // 27 = escape
      openedSlider.style.display = "none";
      openedSlider = null;
    }
  }
});
