function arrayRemove(arr, value) {
    // Fonction pour enlever un élement d'une array
    return arr.filter(function(ele) {
        return ele != value;
    });
}

function resetFiltres() {
    const coche = document.querySelectorAll("input:checked");
    for (let input of coche) {
        input.checked = false;
    }

    actives = [];
    clear.style.display = "none";
    display(actives);
    checkNoCompatible();
}

function checkNoCompatible() {
    // Fonction pour "griser" les filtres inutiles qui n'afficheront rien une fois qu'un
    // filtre a déjà été sélectionné

    // Les titres sont pas dans la partie tech alors il faut trouver le bon
    // sélecteur
    let allCats = document.querySelectorAll(".project:not(.hidden) .value:not(.pastille)");

    let catsCompatibles = [];
    [...allCats].forEach(function(el) {

        let catName;
        if (!el.previousSibling) {
            catName = el.parentElement.parentElement.previousSibling.previousSibling.innerHTML;
        } else if (el.tagName == 'DIV') {
            catName = 'titre:';
        } else if (el.previousSibling.classList.contains("pastille")) {
            catName = el.previousSibling.parentElement.parentElement.previousSibling.previousSibling.innerHTML.trim();
        } else {
            catName = el.previousSibling.innerHTML.trim();
        }

        if (catName.includes("papier")) {
            catName = "papier";
        }

        if (catName.includes("couleur")) {
            catName = "couleur";
        }

        // Bien normaliser tout ça pour que les noms des classes soient les mêmes
        // partout
        let cleanName = (catName + ':' + el.innerHTML).trim().replaceAll(' ', '_').replace('_:', ':').replace('.', '-').toLowerCase().replace('&amp;', '&');
        // console.log('cleanName :',cleanName);

        // On doit échapper les liens possibles
        if (cleanName.includes('href="http')) {
            catsCompatibles.push(cleanName.replace(/<a.*?>(.*?)<\/a>/g, '$1'));
        } else {
            catsCompatibles.push(cleanName);
        }
    });

    // Enlève les doublons dans la liste
    catsCompatibles = [...new Set(catsCompatibles)];

    let inputs = document.querySelectorAll("nav input");
    [...inputs].forEach(function(input) {
        // Ajoute une classe aux inputs qui ne se retrouvent pas dans les projets déjà affichés
        input.classList.remove("non-pertinent"); // reset
        // console.log('input :',input.value);

        if (!catsCompatibles.includes(input.value)) {
            // console.log('input :',input.value);
            input.classList.add("non-pertinent");
        }

    });
}

function checkFiltres(ev) {
    // Fonction pour vérifier quels filtres sont sélectionnés
    let filtre = ev.target;
    let filterString = filtre.id.replaceAll(" ", "_").replaceAll(".", "-");

    if (filtre.checked) {
        actives.push(filterString);
        // let onOff = 1;
    } else {
        actives = arrayRemove(actives, filterString);
    }

    if (actives.length > 0) {
        clear.style.display = "block";
    } else {
        clear.style.display = "none";
    }

    if (query) {
        document.getElementById('categories').style.display = 'none';
    }

    console.log(actives);

    display(actives);
    checkNoCompatible();
}

const mediaQuery = window.matchMedia('(max-width: 800px)')

function handleMobileChange(e) {
    // Check if the media query is true
    if (e.matches) {
        query = true;
    } else {
        query = false;

    }
}

let query = false;
// Register event listener
mediaQuery.addListener(handleMobileChange)

// Initial check
handleMobileChange(mediaQuery)


function display(actives) {
    // Fonction pour afficher tous les projets qui contiennent toutes les entrées actives
    [...projects].forEach(function(proj, i) {
        if (actives.length > 0) {
            proj.classList.remove('hidden'); // reset

            // On fait une liste qui va vérifier que tous les filtres sont bien présents en MÊME TEMPS pour afficher le projet
            let cébon = [];

            actives.forEach(function(active) {
                const catEntry = active.split(':');
                const cat = catEntry[0];
                const entry = catEntry[1];

                if (catSubList.includes(cat)) {

                    matches = Object.values(dict[i]).filter(s => s.includes(cat))[0];
                    // console.log(matches);

                    if (typeof (matches) !== 'undefined') {
                        if (matches.includes(',')) {
                            const listeEntries = matches.replace(cat, '').split(',');
                            if (listeEntries.includes(entry)) {
                                cébon.push(true);

                            } else {
                                cébon.push(false);
                            }

                        } else if (matches.replace(cat, '') == entry) {
                            cébon.push(true);

                        } else {
                            cébon.push(false);
                        }

                    } else {
                        cébon.push(false);
                    }

                } else {
                    if (Object.values(dict[i]).includes(active.replace(':', ''))) {
                        cébon.push(true);
                    } else {
                        cébon.push(false);
                    }
                }
            });

            // On change le plan, maintenant on ajoute une classe hidden pour cacher
            // les éléments non voulus

            // S'il y a une seule entrée qui n'est pas dans la liste cébon alors on affiche pas le projet
            if ((cébon.includes(false))) {
                proj.classList.add('hidden');
            } else {
                proj.classList.remove('hidden');
            }

        } else {
            proj.classList.remove('hidden');
        }

    });
}

const filtres = document.querySelectorAll('input[type^="checkbox"]');
const clear = document.getElementById('clear');
const projects = document.getElementsByTagName('article');
const infosTechniques = document.querySelectorAll('.popup .tech .liste');
const catSubList = ['couleur', 'papier', 'façonnage', 'impressions'];
let actives = [];

// Range les infos de chaque projet dans une variable pour qu'on puisse les afficher s'ils possèdent une des entrées/filtres sélectionné·es dans la nav
let dict = {};

infosTechniques.forEach(function(project, index) {
    let keyValues = project.children;

    // let titre = project.previousSibling.previousSibling.innerHTML;
    // console.log(' ');
    // console.log(`---PROJET → ${index} → ${titre} ---------------------------------`);

    // Dictionnaire qui contient tous les projets avec toutes les catégories et
    // les entrées correspondantes
    dict[index] = [];

    // Pour les catégories fusionnées
    let nom_entree;
    let tempEntries = { "couleur": [], "papier": [] };

    for (let el of keyValues) {
        let entries;
        const exceptionList = ['nb exemplaires', 'nb pages', 'nb version', 'type trame', 'ex par version', 'sens coulée', 'sous-traitance'];
        const fusionList = {
            "couleur": ['couleurs couv', 'couleurs recto', 'couleurs verso'],
            "papier": ['papiers interieur', 'papier couverture']
        };

        const category = el.children[1].innerHTML.trim();
        const main_category = category.split(" ")[0].replace(/s$/, '');

        if (exceptionList.includes(category)) {
            continue

        } else if (Object.keys(fusionList).includes(main_category)) {

            if(category == "papier couverture") {
                tempEntries[main_category].push(el.children[2].innerHTML.trim());
            }

            for (let li of el.children[2].children) {
                tempEntries[main_category].push(li.children[0].innerHTML.trim());
            }

        } else {
            if (el.children[2].tagName == 'UL') {
                let joinList = '';
                for (let li of el.children[2].children) {
                    joinList += ',' + li.children[0].innerHTML.trim();
                }

                entries = joinList.replaceAll(' ', '_').replaceAll('.', '-');

            } else {
                entries = el.children[2].innerHTML.trim();
            }
        }

        // console.log(nom_entree);
        nom_entree = (category + entries).toLowerCase().replaceAll(' ', '_').replaceAll('.', '-');

        // On doit échapper les possibles liens avec str.replace()
        if (nom_entree.includes('href="http')) {
            dict[index].push(nom_entree.replace(/<a.*?>(.*?)<\/a>/g, '$1'));

        } else if (!Object.keys(fusionList).includes(main_category)) {
            dict[index].push(nom_entree);

        }

    }

    for (let category in tempEntries) {
        const entriesList = [...new Set(tempEntries[category])]; // enlève les duplicates
        nom_entree = (category + entriesList.join(',')).toLowerCase().replaceAll(' ', '_').replaceAll('.', '-');
        // console.log(nom_entree);
        dict[index].push(nom_entree);

        // if (titre.includes("Projection")) {
        //     console.dir(dict[index]);
        // }
        }
    });

// Relance le script d'affichage chaque fois qu'on coche ou décoche un filtre
filtres.forEach(function(filtre) {
    filtre.addEventListener("click", checkFiltres);
});
