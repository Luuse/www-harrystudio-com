# www-harrystudio-com

Site de HarryStudio

Pour modifier et mettre à jour le site il y a plusieurs étapes :
+ Télécharger le code source
	- Avec Git :
		+ installer [Git](https://git-scm.com/download/win)
		+ cloner [ce repo](https://gitlab.com/Luuse/www-harrystudio-com) Git (cliquer sur "cloner" à droite de l'écran, puis > "cloner avec HTTPS")
		+ faire `git clone https://gitlab.com/Luuse/www-harrystudio-com` dans le terminal dans le dossier de son choix
	- Sans Git :
		+ télécharger une [archive du repo](https://gitlab.com/Luuse/www-harrystudio-com/-/archive/main/www-harrystudio-com-main.zip)
		+ dézipper l'archive dans le dossier de son choix

Ensuite il y a plusieurs endroits à modifier :
+ le dossier `images`, où il faut mettre toutes les images d'un projet dans un dossier
+ `index.csv`, avec toutes les infos techniques et le chemin des images
+ `about.md`, avec les infos qui vont dans le "'a propos" (bouton "?"). La syntaxe est le [markdown](https://www.markdownguide.org/basic-syntax).
+ `secret.txt`, avec les infos de connexion pour le ftp
+ `site/favicon.ico`, pour avoir une jolie icône

Il est aussi possible de modifier le template dans `static_generation/ressources/templates/main.html` et le css dans `site/main.css`

Il est possible d'ajouter des liens dans les catégories `client·es` et `auteur·ices`, mais il faut mettre le tag html en entier → `<a href="LIEN">NOM<a>`

Ensuite il faut installer python3 et pip, puis lancer `python3 synchro.py` dans le terminal depuis le dossier du code source, et c'est parti !
