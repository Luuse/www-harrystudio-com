from jinja2 import Environment, FileSystemLoader
import csv
import re
import json
import os
import itertools
import markdown
import lxml.etree as ET


def readCSV(file):
    with open(file) as csvfile:
        spamreader = csv.reader(csvfile, delimiter=",")
        csvdata = list(spamreader)
        return csvdata


def readSVG(file):
    xml = ET.parse(file)
    svg = xml.getroot()
    svg_width = svg.attrib["viewBox"].split(" ")[
        2
    ]  # Accède à la viewBox et copie seulement la largeur
    path = svg.find(".//{http://www.w3.org/2000/svg}path").attrib["d"]
    return svg_width, path


def get_svgs_infos(path):
    result = []
    for p in os.listdir(path):
        result.append(readSVG("%s/%s" % (path, p)))
    return result


def clean_links(link):
    return re.sub(r"<a.*?>(.*?)</a>", r"\1", link)


class StaticSite:
    def __init__(self):
        self.templatesDir = "static_generation/ressources/templates/"
        self.defaultTemplateFile = "main.html"
        self.siteDir = "site"
        # self.statics = "statics"
        self.port = 8888

        self.items = {}
        self.cat_list = {}

        # une liste des catégories où il y a des valeurs non communiquées
        self.cat_with_NC = ([])
        self.fusion = [6, 7, 10, 11, 12]

        self.projects = []
        self.svg_paths = get_svgs_infos("static_generation/ressources/cartoTitle")
        self.dump = readCSV("index.csv")
        self.categories = self.dump[1]
        self.dump.pop(0)
        self.dump.pop(0)

        with open("about.md") as mdfile:
            self.about = markdown.markdown(mdfile.read())

        with open("static_generation/ressources/riso-colors/color_codes.json", "r") as f:
            self.color_codes = json.loads(f.read())

        with open("static_generation/ressources/cat_codes.json", "r") as f2:
            self.cat_codes = json.loads(f2.read())

        for cat in self.categories:
            if "papier" in cat or "couleur" in cat:
                main_cat = cat.split(" ")[0].rstrip("s")
                self.items[main_cat] = []
            else:
                self.items[cat] = []

    def createCategories(self):
        # Rangement dans le dictionnaire self.cat_list qui contient toutes
        # les catégories et les entrées possibles pour chaque catégorie
        for i, cat in enumerate(self.categories):
            if i not in [0]:
                for ligne in self.dump:

                    # Certaines catégories peuvent avoir plusieurs valeurs
                    if i in [4, 13, 6, 7, 10, 11, 12]:
                        temp = ligne[i].lower().split(",")
                        temp = list(map(str.strip, temp))
                        liste_propre = list(filter(None, temp))

                        # Certaines catégories sont fusionnées
                        if i in self.fusion:
                            main_cat = cat.split(" ")[0].rstrip("s")
                            # print(main_cat, liste_propre)
                            self.items[main_cat].append(liste_propre)
                        else:
                            self.items[cat].append(liste_propre)

                    else:
                        if ligne[i]:
                            self.items[cat].append(ligne[i])
                        else:
                            self.cat_with_NC.append(cat)

        for j, category in enumerate(self.categories):
            if j not in [0]:

                if j in self.fusion:
                    cat = category.split(" ")[0].rstrip("s")
                else:
                    cat = category

                # Enlève les doublons
                if isinstance(self.items[cat][0], list):
                    alors = list(
                        itertools.chain.from_iterable(self.items[cat]))
                    bonne_liste = list(dict.fromkeys(alors))
                else:
                    bonne_liste = list(dict.fromkeys(self.items[cat]))

                # Trie les entrées
                if j in [2, 14, 15, 16, 17]:
                    bonne_liste = sorted(bonne_liste, key=int)
                else:
                    bonne_liste = sorted(bonne_liste)

                self.cat_list[cat] = bonne_liste

                # Ajout de N/C à la fin de la liste des entrées s'il y a lieu
                if cat in self.cat_with_NC:
                    self.cat_list[cat].append("N/C")

    def createProjects(self):
        # Rangement dans la liste self.projects qui contient tous les projets,
        # et les images sous forme de liste de chemins relatifs
        for ligne in self.dump:
            nvl_ligne = []
            liste_images = []

            for img in sorted(os.listdir(f"images/{ligne[2]}/{ligne[0]}")):
                liste_images.append("../images/%s/%s/%s" %
                                    (ligne[2], ligne[0], img))
            nvl_ligne.append(liste_images)

            ligne.pop(0)

            for index, element in enumerate(ligne):
                if element:
                    if index in [3, 5, 9, 10, 11, 12]:
                        liste = element.split(",")
                        nvl_ligne.append(list(map(str.strip, liste)))

                    else:
                        nvl_ligne.append(element)
                else:
                    nvl_ligne.append("N/C")

            self.projects.append(nvl_ligne)

    def templateToSite(self):
        file_loader = FileSystemLoader("static_generation/ressources/templates")
        env = Environment(loader=file_loader)
        env.filters["clean_links"] = clean_links

        # INDEX
        template = env.get_template("main.html")

        output = template.render(
            categories=self.categories,
            cat_list=self.cat_list,
            projects=self.projects,
            colors=self.color_codes,
            svg_paths=self.svg_paths,
            cat_codes=self.cat_codes,
            about=self.about,
        )
        with open(self.siteDir + "/index.html", "w") as fh:
            fh.write(output)

    def build(self):
        self.createCategories()
        self.createProjects()
        self.templateToSite()
