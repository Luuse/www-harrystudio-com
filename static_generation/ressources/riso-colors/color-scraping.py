#!/usr/bin/env pyhton3

from bs4 import BeautifulSoup
import json

with open('base.html', 'r') as f:
    html = f.read()

soup = BeautifulSoup(html, 'lxml')

# titres = soup.select('div[class^="view-rows"] div[class="color-meta"] span[class="field-content"]')

titres = soup.select('span[class^="field-content"] a')
hex_codes = soup.select('div.views-field-field-ink-color-hex div.field-content')

color_codes = {}
for i, titre in enumerate(titres[:-1]):
    color_codes[titre.decode_contents().lower()] = '#'+hex_codes[i].decode_contents().lstrip('#').upper()


with open('color_codes.json', 'w') as file:
    file.write(json.dumps(color_codes))
