          <li>
            <h3> <span class="icone_cat">{{ cat_codes[category] }}</span> {{ category }} </h3>
            <ul class="entries">
              {%- for entry in cat_list[category] -%}
                {%- set alors = (category + ':' + entry) | clean_links | replace(" ", "_") | replace(".", "-") | lower -%}

                {%- if 'couleur' in category -%}
                  {%- if colors[entry|clean_links] -%}
                    <li class="filtre {% if entry == 'cmjn' %}cmjn{% elif entry == 'white' %} white{% endif %}" style="--local-color: #{{ colors[entry] }};">

                      <input type="checkbox" id="{{ alors }}" {{ category }}" value="{{ alors }}">

                      <label for="{{ alors }}">
                        <span class="value text-coul">{{ entry|clean_links }} </span>
                      </label>

                    </li>
                  {%- else -%}
                    <li class="error">{{ entry }}</li>
                  {%- endif -%}

                {%- else -%}
                  {%- if entry != 'N/C' -%}
                    <li class="filtre">

                      <input type="checkbox" id="{{ alors }}" class="filtre {{ category | replace(' ', '_') | replace('.', '-') | lower }}" value="{{ alors }}">
                      <label for="{{ alors }}">
                        {{ entry|clean_links }}
                      </label>

                    </li>
                  {%- endif -%}
                {%- endif -%}
               {%- endfor -%}
            </ul>
          </li>
