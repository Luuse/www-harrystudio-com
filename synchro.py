#!/usr/bin/python

import subprocess
import sys


def initial_install():
    subprocess.check_call(
        [sys.executable, "-m", "pip", "install", "-r", "requirements.txt"]
    )


try:
    import static_generation
    from ftpsync.targets import FsTarget
    from ftpsync.ftp_target import FTPTarget
    from ftpsync.synchronizers import UploadSynchronizer

except ImportError:
    initial_install()
    import static_generation
    from ftpsync.targets import FsTarget
    from ftpsync.ftp_target import FTPTarget
    from ftpsync.synchronizers import UploadSynchronizer


print("\n1 TOUT EST BIEN INSTALLÉ ----\n")

alors = static_generation.StaticSite()
alors.build()
print("\n2 TOUT EST BIEN GÉNÉRÉ ----\n")

local = FsTarget("site")

with open("secrets.txt", "r", encoding="utf-8") as f:
    contenu = f.read().splitlines()
    user = contenu[0].split("=")[-1]
    passwd = contenu[1].split("=")[-1]
    ftp = contenu[2].split("=")[-1]

remote = FTPTarget("/www", ftp, username=user, password=passwd)
opts = {"force": True, "delete_unmatched": True, "verbose": 3}
s = UploadSynchronizer(local, remote, opts)
s.run()

local2 = FsTarget("images")
s = UploadSynchronizer(local, remote, opts)
s.run()
print("\n3 TOUT EST BIEN ENVOYÉ ----\n")
